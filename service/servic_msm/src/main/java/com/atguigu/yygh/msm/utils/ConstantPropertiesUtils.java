package com.atguigu.yygh.msm.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConstantPropertiesUtils implements InitializingBean {

    @Value("${sms.templateId}")
    private String templateId;

    @Value("${sms.appCode}")
    private String appCode;

    public static String TEMPLATE_ID;
    public static String APP_CODE;

    @Override
    public void afterPropertiesSet() throws Exception {
        TEMPLATE_ID=templateId;
        APP_CODE=appCode;
    }
}

